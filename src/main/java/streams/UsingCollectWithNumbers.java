package streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class UsingCollectWithNumbers {
    public static void main(String[] args) {
        List<String> strings = Arrays.asList(
                "9", "88", "42", "1098", "14", "33");

        List<Integer> evens = strings.stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        System.out.println(evens);
    }
}
